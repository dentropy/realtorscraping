#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import os
class LCBOBot():
    def __init__(self):
        self.setup()

    def setup(self):
        self.options = Options()
        self.options.headless = False
        self.driver = webdriver.Firefox(options=self.options)
        self.setup_state = True
    
    def shutdown(self):
        self.setup_state = False
        self.driver.close()
        return(True)

my_bot = LCBOBot()